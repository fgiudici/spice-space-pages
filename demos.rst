.. _top:

Demos
#####

:slug: demos

.. contents:: Table of Contents

Running graphic application in a container using Xspice
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

Runing Xspice in a container is straightforward, here's the docker file used for this example:

.. code-block:: dockerfile

 FROM fedora:latest
 RUN dnf install -y xorg-x11-server-Xspice
 EXPOSE 5900/tcp
 ENV DISPLAY=:1.0
 CMD Xspice --port 5900 --disable-ticketing $DISPLAY  > /dev/null 2>&1 &  /usr/bin/bash

**Steps:**

1. Pull and Run the Xspice container:
    .. code-block:: bash

      podman run -p 5900:5900 -it quay.io/spice/xspice:latest

2. Install and run graphic application
    .. code-block:: bash

      dnf install -y tuxmath && tuxmath

3. Connect with spice
    .. code-block:: bash

      remote-viewer spice://localhost:5900

.. youtube:: NbCc7KR-bGc
    :width: 800
    :height: 450


`back to top <#top>`_

3D graphics acceleration using virgl and local spice
++++++++++++++++++++++++++++++++++++++++++++++++++++

The idea is to use (any) host GPU to accelerate guest graphics
(similar to other host processes).

Guest GPU is a paravirtual device (virtio-vga) that
is used to pass graphics commands to be executed on the host GPU
(via qemu and virgl-renderer)

Guest framebuffer information is then passed by qemu-kvm to spice-server (library)
which sends it to spice client.

Local spice means that the spice client is running on the same
machine on which the VM is running (the client machine is the host).

In the local spice case, unix-domain-socket is used and the guest framebuffer stays on the GPU.


.. _Spice User Manual: spice-user-manual.html#_gl_acceleration_virgl

For more information about configuration check out `Spice User Manual`_


**What is shown in the demo:**

1. Run a VM with virtio-vga, virgl and local spice

2. Connect to the VM using remote-viewer

3. Simple terminal and editors -- vi and gedit

4. Run firefox - show maps.google.com, shadertoy (show also on client), regular browsing

5. Note that copy/paste works [same as in other demos]


.. youtube:: vjuL97lfKCk
    :width: 800
    :height: 450


`back to top <#top>`_

SPICE Adaptive Streaming: performance recording and benchmarking
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


As part of the SPICE Adaptive Streaming project, we developed a toolbox for studying the performance of real-time video streaming. The toolbox consists in:

(1) a recording infrastructure that collects performance indicators in the guest/host/client systems;

(2) a scripted benchmarking engine that controls the systems' configuration and the video encoding settings, and benchmarks each set of parameters one by one;

(3) a graph visualization GUI that plots the benchmarks results and allows studying the impact of each parameter variations.

In the current step, we are working on a mathematical modelisation of the resource usage (CPU, GPU, etc.)

Repository: https://gitlab.com/kpouget_spice/streaming_stats
Full version of the recording (35min): https://youtu.be/vdcFiyhFvUw

.. youtube:: MzJiE8pvVFQ
    :width: 800
    :height: 450


`back to top <#top>`_

Accelerated Spice streaming with Intel
++++++++++++++++++++++++++++++++++++++

This is remote host-guest setup that is configured with spice-streaming-agent and Intel's GVTg technology.

The video demonstrates how the guest can utilizes its GPU in order to run graphics-intensive loads and to
accelerate the encoding process of the video stream that is sent to the client.

For more information about configuration check out `GVTg <spice-user-manual.html#gvtg>`_.

.. youtube:: bpeV4Z-MjKg
    :width: 800
    :height: 450

`back to top <#top>`_
